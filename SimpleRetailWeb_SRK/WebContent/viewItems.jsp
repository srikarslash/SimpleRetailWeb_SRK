<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Products</title>
</head>
<body>

	<c:choose>
		<c:when test = "${show == '1'}" >
        
	    <form action="./Product" method="post">
			<table>
				<tr>
					<td></td>
					
			    	<td>UPC</td>
			    	<td>Description</td>
			        <td>Price</td>
			        <td>Weight</td>
			        <td>Shipping Method</td>
			        
			    </tr>
			<c:forEach items="${myItems}" var="myItems">
			    <tr>
			    	<td><input type="checkbox" name="itemProperties" value="${myItems.upc}/${myItems.description}/${myItems.price}/${myItems.weight}/${myItems.shipping}" >
			    	<td>${myItems.upc}</td>
			    	<td>${myItems.description}</td>
			        <td>${myItems.price}</td>
			        <td>${myItems.weight}</td>
			        <td>${myItems.shipping}</td>
			        
			    </tr>
			</c:forEach>
			</table><br/>
			<input type="submit" value="Submit Items" name="second">
		</form>
        
        </c:when>
        
        <c:otherwise >
        	<table>
        		<tr>
			    	<td>UPC</td>
			    	<td>Description</td>
			        <td>Price</td>
			        <td>Weight</td>
			        <td>Shipping Method</td>
			        <td>Shipping Cost</td>
			    </tr>
			<c:forEach items="${newItems}" var="myItems">
			    <tr>			    
			    	<td>${myItems.upc}</td>
			    	<td>${myItems.description}</td>
			        <td>${myItems.price}</td>
			        <td>${myItems.weight}</td>
			        <td>${myItems.shipping}</td>
			        <td>${myItems.shippingCost}</td>
			    </tr>
			</c:forEach>
			</table><br/>
			<form action="index.html" method="post">
		    	<input type="submit" value="Home"/>
			</form>
        
        </c:otherwise>
    
	
	</c:choose>
	
	
</body>
</html>