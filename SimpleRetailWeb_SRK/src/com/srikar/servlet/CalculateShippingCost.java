package com.srikar.servlet;

public interface CalculateShippingCost {
	
	public double calculateShipping(double weight, long upc);	
	
}
