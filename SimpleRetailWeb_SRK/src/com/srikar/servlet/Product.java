package com.srikar.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





/**
 * Servlet implementation class Product
 */
public class Product extends HttpServlet {
	
	public ArrayList<Item> myItems;

	
	public void init(ServletConfig config) throws ServletException {
		
		myItems = new ArrayList<Item>();

		myItems.add(new Item(477321101878L, "iPhone -  Headphones", 17.25, 3.21, ShippingMethod.GROUND));
		myItems.add(new Item(567321101987L, "CD � Pink Floyd, Dark Side Of The Moon", 19.99, 0.58, ShippingMethod.AIR));
		myItems.add(new Item(567321101986L, "CD � Beatles, Abbey Road", 17.99, 0.61, ShippingMethod.GROUND));
		myItems.add(new Item(567321101985L, "CD � Queen, A Night at the Opera", 20.49, 0.55, ShippingMethod.AIR));
		myItems.add(new Item(567321101984L, "CD � Michael Jackson, Thriller", 23.88, 0.50, ShippingMethod.GROUND));
		myItems.add(new Item(467321101899L, "iPhone - Waterproof Case", 9.75, 0.73, ShippingMethod.AIR));
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("first") != null){
			
		
			request.setAttribute("myItems",myItems);
			request.setAttribute("show", "1");
			
			RequestDispatcher rd= request.getRequestDispatcher("viewItems.jsp");
			rd.forward(request, response);
		}	
		
		else if(request.getParameter("second") != null) {
			
			ArrayList<Item> newItems = new ArrayList<Item>();
			
			String[] itemProperties = request.getParameterValues("itemProperties");
			for(String item : itemProperties) {
				String[] x = item.split("/");
				
				newItems.add(new Item(Long.parseLong(x[0], 10), x[1], Double.parseDouble(x[2]),Double.parseDouble(x[3]), ShippingMethod.valueOf(x[4])));
				
			}
			
			
			request.setAttribute("newItems",newItems);
			
			RequestDispatcher rd= request.getRequestDispatcher("viewItems.jsp");
			rd.forward(request, response);
			
		}

		
		
		
		
		
	}

}
