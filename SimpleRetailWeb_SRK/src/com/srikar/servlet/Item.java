package com.srikar.servlet;

public class Item {
	
	private long upc;
	private String description;
	private double price;
	private double weight;
	private ShippingMethod shipping;
	private double shippingCost;

	public Item() {
		// TODO Auto-generated constructor stub
	}
	
	public Item(long upc, String description, double price, double weight, ShippingMethod shipping) {
		
		this.upc = upc;
		this.description = description;
		this.price = price;
		this.weight = weight;
		this.shipping = shipping;
		
		this.calShippingCost();
	}
	
	public double getShippingCost() {
		return this.shippingCost;
	}

	public long getUpc() {
		return this.upc;
	}

	public String getDescription() {
		return this.description;
	}

	public double getPrice() {
		return this.price;
	}

	public double getWeight() {
		return this.weight;
	}

	public ShippingMethod getShipping() {
		return this.shipping;
	}

	private void calShippingCost() {
		if(shipping == ShippingMethod.AIR) {
			//use air
			AirShippingCost cost = new AirShippingCost();
			this.shippingCost = Math.round((cost.calculateShipping(this.weight, this.upc))*100.0)/100.0;
		}else {
			//use ground
			GroundShippingCost cost = new GroundShippingCost();
			this.shippingCost = Math.round((cost.calculateShipping(this.weight, this.upc))*100.0)/100.0;
		}
	}
	
	@Override
	public String toString() {
		return "Item [upc=" + upc + ", description=" + description + ", price=" + price + ", weight=" + weight
				+ ", shipping=" + shipping + "]";
	}
	
	
		
	
}



